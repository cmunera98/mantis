<?php
/**Direccion Mantis */
$g_path                   = 'https://mantis.choucairtesting.com/Choucair_BugTracker/';
$g_default_language       = 'spanish';

/**Conexion a la Base de Datos */
$g_hostname               = '10.180.119.3';
$g_db_type                = 'mysqli';
$g_database_name          = 'mantis';
$g_db_username            = 'connection';
$g_db_password            = 'Choucair2019**';

$g_default_timezone       = 'UTC';

$g_crypto_master_salt     = 'O8gOH6t6v6Ot3WcSsqZ7pYYlGsz0Hjo95i/eIRyAfWc=';
$g_logo_image = 'images/logo.png';
$g_window_title = 'CHOUCAIR TRACKER';
$g_copyright_statement = 'Choucair Cárdenas Testing. Todos los derechos reservados – 2018. Choucair ©';
$g_allow_signup			= OFF;
$g_allow_permanent_cookie = OFF;
$g_max_file_size = 28388608;

/**Logging and Debug mantis */
$g_show_timer	= ON;
$g_show_memory_usage = ON;


/**Configuracion de Conexion LDAP */
#$g_login_method = LDAP;
#$g_ldap_protocol_version = 3;
#$g_ldap_server = 'ldap://172.17.45.2:389/';
#$g_ldap_root_dn = 'DC=corp,DC=choucairtesting,DC=local';
#$g_ldap_bind_dn = 'CN=admindav,CN=Users,DC=corp,DC=choucairtesting,DC=local'; 
#$g_ldap_bind_passwd = '**//Sigueadelante*';
#$g_ldap_organization = '(memberOf=CN=Intranet,OU=Grupos,DC=corp,DC=choucairtesting,DC=local)';
#$g_use_ldap_realname = ON;
#$g_ldap_realname_field = 'sn';
#$g_ldap_uid_field = 'samaccountname';
#$g_use_ldap_email = ON;
#$g_ldap_follow_referrals = OFF;

/**Notificaciones de Email */
/** Log level, configura los logs de auditoria de correo electronico se puede leer en event log plugin */
$g_log_level = LOG_EMAIL | LOG_EMAIL_RECIPIENT | LOG_LDAP;
#$g_log_destination = 'file:E:\Apps\htdocs\Choucair_Tracker\log\mantisbt.log';
$g_webmaster_email	= 'scardonas@choucairtesting.com';

/**Activa la notificacion por email */
$g_enable_email_notification	= ON;


/** Configuracion por SMTP */
$g_phpMailer_method = PHPMAILER_METHOD_SMTP; 
$g_smtp_host = 'smtp.office365.com'; 
$g_smtp_username = 'portales@choucairtesting.com'; 
$g_smtp_password = '2pzULTHvN1'; 
$g_smtp_connection_mode = 'tls'; 
$g_smtp_port = 587;
$g_from_name = 'Choucair Bug Tracker'; 
$g_from_email = 'portales@choucairtesting.com'; 
$g_return_path_email = 'portales@choucairtesting.com'; 


/** *****************************Configuracion predeterminada de metologia ******************************************/
/** Toda la configuracion de Traduccion se encuentra en el archivo 'custom_strings_inc' dentro de la carpeta config*/

/**Severidad -> */
/**Buscar la siguiente linea en el archivo ->>
 * $s_severity_enum_string = ''; */
$g_severity_enum_string = '10:Not Established,
                           20:Low,
                           30:Medium,
                           40:High,
                           50:Severe';
$g_default_bug_severity = FEATURE;
/**Prioridad -> */
/**Buscar la siguiente linea en el archivo ->> 
 * $s_priority_enum_string = '';
*/
$g_priority_enum_string = '20:Low,30:Medium,40:High,60:Urgent';

/**************************************Configuracion de Estados a Nivel de Sistema******************************** */
/** Toda la configuracion de Traduccion se encuentra en el archivo 'custom_strings_inc' dentro de la carpeta config*/

/**Estados */
$g_status_enum_string = '10:New,
						 21:Does not Apply,
						 22:Work as designed,
						 23:Next Version,
						 11:For assignment,
						 12:Assigned,
						 13:For analysis,
						 14:In process,
						 15:Resolved,
						 16:Verified,
						 30:Reopened,
						 90:Closed,
						 60:Duplicated,
						 26:Canceled,
						 17:Review by Tester,
						 24:Could not be replicated';

/*Resoluciones*/ 
$g_resolution_enum_string = '10:open,
                             90:fixed,
                             30:reopened,
                             40:unable to duplicate,
                             50:not fixable,
                             60:duplicate,
                             70:not a bug,
                             80:suspended,
                             20:wont fix';  
                                                  
$g_bug_resolution_not_fixed_threshold = array(SUSPENDED);
$g_bug_resolutions_fixed = array (UNABLE_TO_REPRODUCE, NOT_A_BUG, OPEN);
$g_allow_parent_of_unresolved_to_close = ON;
#$g_reauthentication_expiry = 0*60;
/** Colores de los Estados */
$g_status_colors = array(
    'New'                      => '#FF0000', # Rojo 
    'Does not Apply'           => '#6AD1FE', # Azul celeste
    'Work as designed'         => '#FEBC00', # Naranja
    'Next Version'             => '#00FEEA', # Verde fosforescente
    'For assignment'           => '#e3b7eb', # Purpura
    'Assigned'                 => '#006CFF', # Azul  
	'For analysis'             => '#ffcd85', # orange 
	'In process'               => '#F7FF00', # Amarillo 
    'Resolved'                 => '#00FF0F', # Verde 
    'Verified'                 => '#C3FE3C', # Verde Aguamarino
    'Reopened'                 => '#FE3C5A', # Rojo Oscuro
    'Closed'                   => '#979797', # Gris  
    'Duplicated'               => '#009BA5',
    'Canceled'                 => '#000000',
    'Review by Tester'         => '#067E00',
    'Could not be replicated'  => '#5A007C'
);



/******************************************Configuracion del Reporte************************************************* */
/**Contenido del Reporte */
$g_bug_report_page_fields = array(
	'attachments',
	'category_id',
	'due_date',
	'handler',
	'os',
	'os_version',
	'platform',
	'priority',
	'product_build',
	'product_version',
	'reproducibility',
	'steps_to_reproduce',
	'tags',
	'target_version',
	'view_state',
);


/** ************************************Configuración Personalizada de Permisos **************************************/
/** Toda la configuracion de Traduccion se encuentra en el archivo 'custom_strings_inc' dentro de la carpeta config*/

/************* Perfiles del Sistema*****
 * NOTA: En caso de querer agregar mas perfiles al sistema, deberia dirigirse al archivo 'custom_constants_inc' y 
 * Buscar ('Niveles de Acceso'), los rangos varian de 0 a 90. 
 * Luego debera de configurar toda la seccion de permisos en caso de que sea necesario
 * 
 * Buscar la siguiente linea en el archivo ->> 
 * $s_access_levels_enum_string = '';
*/
$g_access_levels_enum_string = '0:Gerente de Servicio,
                                10:Analista de Pruebas, 
                                25:Desarrollador,
                                55:Analista de Soluciones,
                                90:Administrador';

/**Nivel de Acceso minimo para ser notificado cuando se ha creado un nuevo usuario, utilizando el formulario de registro*/
$g_notify_new_user_created_threshold_min = ADMINISTRADOR;

/**Idioma de la página */
$g_default_language = 'english';

/**Nivel de Acceso minimo para visualizar el correo electronico de los usuarios.*/
$g_show_user_email_threshold = GERENTE;

/**Nivel de Acceso minimo que se necesita para ver los nombres reales en la vista del usuario */
$g_show_user_realname_threshold = GERENTE;

/**Nivel de Acceso minimo en el que los usuarios verán la fecha de lanzamiento
 * para versiones de productos. Las fechas se mostrarán al lado de la versión del producto,
 * versión de destino y fijo en los campos de versión. */
$g_show_version_dates_threshold = NADIE;

/**Nivel de Acceso minimo para ver la foto de perfil del usuario*/
$g_show_avatar_threshold = GERENTE;

/**Nivel de Acceso minimo para ver las noticias privadas del proyecto */
$g_private_news_threshold = GERENTE;

/**Nivel de Acceso minimo para ingresar al sistema */
$g_default_new_account_access_level = GERENTE;

/**Nivel de Acceso minimo para ver el resumen del proyecto*/
$g_view_summary_threshold = GERENTE;

/**Nivel de Acceso minimo requerido para aparecer en la lista de usuarios que pueden recibir un recordatorio. */
$g_reminder_receive_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para ver en detalle un bug reportado */
$g_view_sponsorship_total_threshold = GERENTE;

/**Nivel de Acceso minimo, para ver a los usuarios que participan en un problema y la cantidad de aportes de cada uno.*/
$g_view_sponsorship_details_threshold = GERENTE;

/**Nivel de Acceso minimo, para que el usuario participe en la solucion de un problema */
$g_sponsor_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para poder manejar problemas reportados. */
$g_handle_sponsored_bugs_threshold = DESARROLLADOR;

/**Nivel de Acceso minimo para poder asignar un problema  */
$g_assign_sponsored_bugs_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para Reportar un Bug */
$g_report_bug_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para actualizar un Bug */
$g_update_bug_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para visualizar un Bug */
$g_view_bug_threshold = GERENTE;

/**Nivel de Acceso minimo para monitorizar los Bugs */
$g_monitor_bug_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para agregar otros usuarios al monitoreo */
$g_monitor_add_others_bug_threshold = DESARROLLADOR;

/**Nivel de Acceso minimo para eliminar otros usuarios del monitoreo */
$g_monitor_delete_others_bug_threshold = DESARROLLADOR;

/**Nivel de Acceso minimo para visualizar Bugs privados */
$g_private_bug_threshold = DESARROLLADOR;

/**Nivel de Acceso minimo para ser asignado a resolver un Bug */
$g_handle_bug_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para ver las notas (Privadas) de un Bug */
$g_private_bugnote_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para ver en detalle los errores del proyecto*/
$g_view_handler_threshold = GERENTE;

/**Nivel de Acceso minimo para ver la historia del Bug */
$g_view_history_threshold = GERENTE;

/**Nivel de Acceso minimo para enviar un recordatorio desde las páginas de vista de error */
$g_bug_reminder_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para eliminar las revisiones del historial de errores */
$g_bug_revision_drop_threshold = ANASOLUCIONES;

/**Nivel de Acceso minimo para cargar archivos, para adjuntarlos a un Bug */
$g_upload_bug_file_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para agregar notas a un Bug */
$g_add_bugnote_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo en el que un usuario puede editar las notas de Bug de otros usuarios */
$g_update_bugnote_threshold = ANASOLUCIONES;

/**Nivel de Acceso minimo para ver el sitio de Administracion */
$g_manage_site_threshold = ANASOLUCIONES;

/***Nivel de Acceso minimo, en el que se considera que un usuario es un administrador del sitio.
 *  Estos usuarios tienen acceso de "superusuario" a todos los aspectos de MantisBT
 */
$g_admin_site_threshold = ADMINISTRADOR;

########################################## PERMISOS DE PROYECTOS ################################# 

/**Nivel de Acceso minimo para que el usuario pueda editar un proyecto
 * No incluye los permisos de: Agregar o eliminar un proyecto
 */
$g_manage_project_threshold = ANASOLUCIONES;
/**Nivel de Acceso Minimo para Crear un proyecto */
$g_create_project_threshold = ANASOLUCIONES;
/**Nivel de Acceso minimo para crear un proyecto privado */
$g_private_project_threshold = ADMINISTRADOR;
/**Nivel de Acceso minimo para eliminar un proyecto */
$g_delete_project_threshold = ADMINISTRADOR;

/**Nivel de Acceso minimo, para administrar el acceso de los usuarios a un proyecto */
$g_project_user_threshold = ANASOLUCIONES;

/**Nivel de Acceso minimo para cargar archivos en la sección de documentación del proyecto */
$g_upload_project_file_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para ver la documentacion del proyecto */
$g_view_proj_doc_threshold = GERENTE;

########################################### FIN DE PERMISOS DE PROYECTOS ########################

/**Nivel de Acceso minimo para que el usuario pueda: 
 * Agregar, editar o eliminar noticias para su grupo de trabajo.
 */
$g_manage_news_threshold = ANASOLUCIONES;

/**Nivel de Acceso minimo para administrar las cuentas de usuario*/
$g_manage_user_threshold = ADMINISTRADOR;

/**Nivel de Acceso minimo para suplantar a un usuario. */
$g_impersonate_user_threshold = ADMINISTRADOR;

/**Nivel de Acceso minimo para eliminar un bug */
$g_delete_bug_threshold = ANASOLUCIONES;

/**Nivel de Acceso minimo para mover un bug */
$g_move_bug_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo  para establecer el estado, mientras informa un error o una nota de error. */
$g_set_view_status_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para actualizar el estado. */
$g_change_view_status_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para mostrar la lista de usuarios que monitorean un error en las páginas de vista de error. */
$g_show_monitor_list_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para poder usar consultas almacenadas */
$g_stored_query_use_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para usar consultas almacenadas */
$g_stored_query_create_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para crear cosultas almacenadas */
$g_stored_query_create_shared_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para actualizar errores de solo lectura. */
$g_update_readonly_bug_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para ver las notas de cambios del proyecto*/
$g_view_changelog_threshold = GERENTE;

/**Nivel de Acceso minimo para ver la línea de tiempo del proyecto */
$g_timeline_view_threshold = GERENTE;

/**Nivel de Acceso minimo para ver la hoja de ruta del proyecto*/
$g_roadmap_view_threshold = GERENTE;

/**Nivel de Acceso minimo para actualizar la hoja de ruta, la versión de destino, etc. */
$g_roadmap_update_threshold = ANASOLUCIONES;

/**Nivel de Acceso minimo para cambiar estados */
$g_update_bug_status_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para reabrir un bug */
$g_reopen_bug_threshold = ANASOLUCIONES;

/**Nivel de Acceso minimo para asignar errores a versiones de productos no lanzados */
$g_report_issues_for_unreleased_versions_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para establecer un error repetitivo*/
$g_set_bug_sticky_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para que alguien sea miembro del equipo de desarrollo */
$g_development_team_threshold = GERENTE;

/**Nivel de Acceso necesario para acceder a un estado, si no esta en la lista vuelve a: */
$g_set_status_threshold = array( NEW_ => ANAPRUEBAS );

/*** Nivel de Acceso necesario para que los usuarios puedan administrar la configuración de un proyecto.
 *   Esto incluye flujo de trabajo, notificaciones por correo electrónico, columnas para ver y otros. */
$g_manage_configuration_threshold = ANASOLUCIONES;

/**Nivel de Acceso necesario para que el usuario pueda ver las configuraciones del sistema */
$g_view_configuration_threshold = ADMINISTRADOR;

/**Nivel de Acceso necesario para el usuario establezca las configuraciones del sistema genéricamente a través del sitio web */
$g_set_configuration_threshold = ADMINISTRADOR;

/**Nivel de Acceso necesario para que los usuarios puedan crear enlaces permanentes. */
$g_create_permalink_threshold = GERENTE;

/**Nivel de Acceso minimo para administrar un campo personalizado */
$g_manage_custom_fields_threshold = ADMINISTRADOR;

/**Nivel de Acceso minimo para agregar o quitar un campo personalizado de un proyecto */
$g_custom_field_link_threshold = ADMINISTRADOR;

/**Nivel de Acceso minimo requerido para ver las etiquetas asociadas a un error */
$g_tag_view_threshold = GERENTE;

/**Nivel de Acceso minimo requerido para adjuntar etiquetas a un error */
$g_tag_attach_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo requerido para separar las etiquetas de un error */
$g_tag_detach_threshold = ANAPRUEBAS;

/**Nivel de acceso requerido para separar las etiquetas unidas por el mismo usuario */
$g_tag_detach_own_threshold = ANAPRUEBAS;

/**Nivel de acceso requerido para crear nuevas etiquetas */
$g_tag_create_threshold = ANAPRUEBAS;

/**Nivel de acceso requerido para editar nombres de etiquetas y descripciones */
$g_tag_edit_threshold = ANASOLUCIONES;

/**Nivel de acceso requerido para editar descripciones de un reporte generado por otro usuario */
$g_tag_edit_own_threshold = ANASOLUCIONES;

/**Nivel de acceso requerido para ver la información del seguimiento del proyecto en el tiempo */
$g_time_tracking_view_threshold = GERENTE;

/**Nivel de acceso requerido para agregar / editar información de seguimiento de tiempo */
$g_time_tracking_edit_threshold = ANASOLUCIONES;

/**Nivel de acceso requerido para generar reportes */
$g_time_tracking_reporting_threshold = ANASOLUCIONES;

/**Nivel de Acceso minimo para agregar la informacion de una nueva plataforma
 * Ejemplo: Microsoft Windows 10
*/
$g_add_profile_threshold = ANAPRUEBAS;

/**Nivel de Acceso minimo para editar las plataformas almacenadas en la Base de Datos */
$g_manage_global_profile_threshold = ADMINISTRADOR;

/**Nivel de Acceso minimo para administrar lo plugins del sistema*/
$g_manage_plugin_threshold = ADMINISTRADOR;

/**Nivel de Acceso minimo para actualizar la fecha del sistema */
$g_due_date_update_threshold = NADIE;

/**Nivel de Acceso minimo para ver la fecha del sistema */
$g_due_date_view_threshold = NADIE;

/**Nivel de Acceso minimo para ver el archivo de logs del sistema */
$g_show_log_threshold = ADMINISTRADOR;

/**Nivel de acceso global mínimo requerido para acceder al servicio web para operaciones de solo lectura.*/
$g_webservice_readonly_access_level_threshold = GERENTE;

/**Nivel de acceso global mínimo requerido para acceder al servicio web para operaciones de lectura / escritura. */
$g_webservice_readwrite_access_level_threshold = ANAPRUEBAS;

/**Nivel mínimo de acceso global requerido para acceder a los servicios web del administrador */
$g_webservice_admin_access_level_threshold = ANASOLUCIONES;

/**Nivel mínimo de acceso al proyecto requerido para poder especificar un nombre dentro del reporte */
$g_webservice_specify_reporter_on_add_access_level_threshold = ANAPRUEBAS;

/**Nivel de acceso necesario para ver los archivos adjuntos de errores */
$g_view_attachments_threshold = GERENTE;

/**Nivel de acceso necesario para descargar los archivos adjuntos de errores */
$g_download_attachments_threshold = GERENTE;

/**Nivel de acceso necesario para eliminar los archivos adjuntos de errores */
$g_delete_attachments_threshold = ANAPRUEBAS;
