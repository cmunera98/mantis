<?php
switch($g_active_language ) {

	case 'english':
        $s_all_projects = 'Global';

        $s_category = 'Product Type';

        $s_priority = 'Priority';

        $s_severity = 'Severety';

        $s_select_profile = 'Plataform';

        $s_priority_enum_string = '20:Low,30:Medium,40:High,60:Urgent';

        $s_access_levels_enum_string = '0:Gerente de Servicio,
                                       10:Desarrollador,
                                       25:Analista de Pruebas, 
                                       55:Analista de Soluciones,
                                       90:Administrador';

        $s_severity_enum_string = '10:Not Established,
                                   20:Low,
                                   30:Medium,
                                   40:High,
                                   50:Severe';

        $s_status_enum_string = '10:New,
								 21:Does not Apply,
								 22:Work as designed,
								 23:Next Version,
								 11:For assignment,
								 12:Assigned,
								 13:For analysis,
								 14:In process,
								 15:Resolved,
								 16:Verified,
								 30:Reopened,
								 90:Closed,
								 60:Duplicated,
								 26:Canceled,
								 17:Review by Tester,
								 24:Could not be replicated';

        $s_allow_reporter_close = "Permitir al Gerente de Servicio Cerrar la Incidencia";
        $s_allow_reporter_reopen = 'Permitir al Gerente de Servicio Reabrir la Incidencia';
        $s_limit_access = 'Limitar el acceso de los Gerentes de Servicio el acceso únicamente a sus propias Incidencias';
        $s_categories = 'Product Type';
        $s_resolution_enum_string = '10:open,
									 90:fixed,
									 30:reopened,
									 40:unable to duplicate,
									 50:not fixable,
									 60:duplicate,
									 70:not a bug,
									 80:suspended,
									 20:wont fix';

		break;

    default:
        $s_all_projects = 'Global';

        $s_category = 'Product Type';

        $s_priority = 'Priority';

        $s_severity = 'Severety';

        $s_select_profile = 'Plataform';

        $s_priority_enum_string = '20:Low,30:Medium,40:High,60:Urgent';

        $s_access_levels_enum_string = '0:Gerente de Servicio,
                                       10:Desarrollador,
                                       25:Analista de Pruebas, 
                                       55:Analista de Soluciones,
                                       90:Administrador';  

        $s_severity_enum_string = '10:Not Established,
								   20:Low,
								   30:Medium,
                                   40:High,
                                   50:Severe';

        $s_status_enum_string = '10:New,
								 21:Does not Apply,
								 22:Work as designed,
								 23:Next Version,
								 11:For assignment,
								 12:Assigned,
								 13:For analysis,
								 14:In process,
								 15:Resolved,
								 16:Verified,
								 30:Reopened,
								 90:Closed,
								 60:Duplicated,
								 26:Canceled,
								 17:Review by Tester,
								 24:Could not be replicated';

        $s_allow_reporter_close = 'Allow Service Manager to close Issue';
        $s_allow_reporter_reopen = 'Allow Service Manager to re-open Issue';
        $s_limit_access = 'Limit Manager\'s Service access to their own issues';
        $s_categories = 'Product Type';
        $s_resolution_enum_string = '10:open,
									 90:fixed,
									 30:reopened,
									 40:unable to duplicate,
									 50:not fixable,
									 60:duplicate,
									 70:not a bug,
									 80:suspended,
									 20:wont fix';

		break;
}

?>

